package main

import (
	"errors"
	"github.com/caarlos0/env/v7"
	"github.com/chenzhijie/go-web3"
	"github.com/chenzhijie/go-web3/eth"
	"github.com/chenzhijie/go-web3/utils"
	"github.com/joho/godotenv"
	"log"
	"math/big"
	"math/rand"
	"os"
	"regexp"
	"time"
)

type Config struct {
	WalletsFilePath string `env:"WALLETS_FILE_PATH"`
	RpcURL          string `env:"RPC_URL"`
	ScannerURL      string `env:"SCANNER_URL"`

	MintTaxPrice string `env:"MINT_TAX_PRICE_GWEI"`
	MintAddress  string `env:"MINT_ADDRESS"`
	MintAbi      string `env:"MINT_ABI"`

	MaxGas float64 `env:"MAX_GAS_GWEI"`

	MinWaitSeconds        int `env:"MIN_WAIT_SEC"`
	MaxWaitSeconds        int `env:"MAX_WAIT_SEC"`
	GasChecksDelaySeconds int `env:"BETTER_GAS_WAIT_SEC"`
}

func main() {
	var (
		cfg = Config{}
	)
	_ = godotenv.Load()

	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("Fail to load config, err:%s \r\n", err)
	}

	wallets, err := getWalletsFromPath(cfg.WalletsFilePath)
	if err != nil {
		log.Fatalf("Fail to load wallets, err:%s \r\n", err)
	}

	web3Client, err := web3.NewWeb3(cfg.RpcURL)
	if err != nil {
		log.Fatalf("Fail to init rpc, err:%s \r\n", err)
	}

	contract, err := web3Client.Eth.NewContract(cfg.MintAbi, cfg.MintAddress)
	if err != nil {
		log.Fatalf("Fail to load contract, err:%s \r\n", err)
	}

	for {
		for address, privateKey := range wallets {
			err = web3Client.Eth.SetAccount(privateKey)
			if err != nil {
				log.Printf("Invalid private key, address: %s, err: %s\r\n", address, err)
				continue
			}

			waitGas(web3Client, cfg.MaxGas, time.Duration(cfg.GasChecksDelaySeconds)*time.Second)

			log.Println("Trying mint, address:", address)
			txHash, err := mint(web3Client, contract, web3Client.Utils.ToWei(cfg.MintTaxPrice))
			if err != nil {
				log.Printf("Failed mint, address: %s, err: %s\r\n", address, err)
			}
			log.Printf("Success mint, address: %s, txHash: %s\r\n", address, txHash)

			waitTime := randTime(cfg.MinWaitSeconds, cfg.MaxWaitSeconds, time.Second)
			log.Printf("Wait delay %v before next mint \r\n", waitTime)
			time.Sleep(waitTime)
		}
	}
}

func mint(client *web3.Web3, contract *eth.Contract, taxWei *big.Int) (string, error) {
	blockNumber, err := client.Eth.GetBlockNumber()
	if err != nil {
		return "", err
	}
	nonce, err := client.Eth.GetNonce(client.Eth.Address(), new(big.Int).SetUint64(blockNumber))
	if err != nil {
		return "", err
	}
	abi, err := contract.EncodeABI("purchase", big.NewInt(1))
	if err != nil {
		return "", err
	}
	fee, err := client.Eth.EstimateFee()
	if err != nil {
		return "", err
	}

	resp, err := client.Eth.SyncSendEIP1559RawTransaction(
		contract.Address(),
		taxWei,
		nonce,
		fee.MaxPriorityFeePerGas.Uint64(),
		fee.MaxPriorityFeePerGas,
		fee.MaxFeePerGas,
		abi,
	)
	if err != nil {
		return "", err
	}
	return resp.TxHash.String(), nil
}

func waitGas(client *web3.Web3, maxGasGwei float64, waitPerIteration time.Duration) {
	for {
		gas, err := client.Eth.GasPrice()
		if err != nil {
			log.Println("fail to get gas price, err:", err)
			continue
		}
		gasPriceWei := new(big.Int).SetUint64(gas)
		gasPriceGwei := client.Utils.FromWeiWithUnit(gasPriceWei, utils.EtherUnitGWei)
		gasPrice, _ := gasPriceGwei.Float64()

		log.Printf("Current gas %f Gwei \r\n", gasPrice)
		if maxGasGwei >= gasPrice {
			break
		}
		log.Println("Wait new gas amount:", waitPerIteration)
		time.Sleep(waitPerIteration)
	}
}

func getWalletsFromPath(path string) (map[string]string, error) {
	fileBody, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	matches := regexp.MustCompile(`(?m)^([^#].*);(.*)$`).FindAllSubmatch(fileBody, -1)
	if len(matches) == 0 {
		return nil, errors.New("no wallets to mint")
	}

	wallets := make(map[string]string, len(matches))
	for _, match := range matches {
		wallets[string(match[1])] = string(match[2])
	}

	return wallets, err
}

func randTime(min, max int, t time.Duration) time.Duration {
	rand.Seed(time.Now().UnixNano())
	return time.Duration(rand.Intn(max-min+1)+min) * t
}
